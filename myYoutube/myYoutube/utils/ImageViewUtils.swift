//
//  ImageViewUtils.swift
//  myYoutube
//
//  Created by Nastya Rogatuk on 18.06.2020.
//  Copyright © 2020 AR. All rights reserved.
//

import Foundation
import UIKit

class CornerViews: UIView {
    
    @IBInspectable var corner: CGFloat = 10{
        didSet{
            self.setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        self.clipsToBounds = true
        self.layer.cornerRadius = corner
        self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
    }
}
