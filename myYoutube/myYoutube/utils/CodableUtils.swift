//
//  CodableUtils.swift
//  myYoutube
//
//  Created by Nastya Rogatuk on 18.06.2020.
//  Copyright © 2020 AR. All rights reserved.
//

import Foundation

enum CodableError: Error, ApplicationError {
    case dataDecodeError(description: String)
    
    func local() -> String {
        switch self {
        case .dataDecodeError(let description):
            return description
        }
    }
}

class CodableService {
    
    static func decode<Entity: Decodable>(data: Data, success:@escaping(ServerSuccessHandlerResult<Entity>), fail:@escaping(ServerErrorHandler)){
        do {
            let decoder = JSONDecoder()
            let result = try decoder.decode(Entity.self, from: data)
            success(result)
        } catch {
            fail(CodableError.dataDecodeError(description: error.localizedDescription))
        }
    }
    
    static func decode<Entity: Decodable>(data: Data) -> Entity? {
        let decoder = JSONDecoder()
        return try? decoder.decode(Entity.self, from: data)
    }
}

extension Data {
    
    var decodeString: String? {
        return String(data: self, encoding: String.Encoding.utf8)
    }
}
