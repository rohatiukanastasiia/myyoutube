//
//  StyleService.swift
//  myYoutube
//
//  Created by Nastya Rogatuk on 18.06.2020.
//  Copyright © 2020 AR. All rights reserved.
//

import Foundation
import UIKit

enum AppColor: String {

    case bannerBackgroundColor
    
    func color() -> UIColor {
        return UIColor(named: self.rawValue)!
    }
}

extension TextStyle {
    static var channelTitle: TextStyle = TextStyle(
        textColor: .black,
        font: UIFont.systemFont(ofSize: 17, weight: .bold)
    )
    
    static var channelSubscriber: TextStyle = TextStyle(
        textColor: .darkGray,
        font: UIFont.systemFont(ofSize: 13, weight: .semibold)
    )
    
    static var playListTitle: TextStyle = TextStyle(
        textColor: .white,
        font: UIFont.systemFont(ofSize: 23, weight: .bold)
    )
    
    static var videoName: TextStyle = TextStyle(
        textColor: .white,
        font: UIFont.systemFont(ofSize: 15, weight: .semibold)
    )
    
    static var videoViews: TextStyle = TextStyle(
        textColor: UIColor.white.withAlphaComponent(0.75),
        font: UIFont.systemFont(ofSize: 11, weight: .regular)
    )
}

//text styling
protocol TextStyling {
    func textStyle(_ value: TextStyle)
}

struct TextStyle {
    var textColor: UIColor
    var font: UIFont
}

extension UILabel: TextStyling {
    func textStyle(_ value: TextStyle) {
        self.textColor = value.textColor
        self.font = value.font
    }
}


