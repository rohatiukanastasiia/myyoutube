//
//  BaseNetworkingModels.swift
//  myYoutube
//
//  Created by Nastya Rogatuk on 18.06.2020.
//  Copyright © 2020 AR. All rights reserved.
//

import Foundation
import Alamofire

typealias Parameters = [String:Any]
typealias ServerErrorHandler = (_ response:ApplicationError)->()
typealias ServerSuccessHandlerResult<Entity: Codable> = (_ result:Entity)->()

typealias SuccessResponse = (_ data: Data)->()

protocol APIConfiguration {
    var method: HTTPMethod { get }
    var path: String { get }
    var query: Parameters? { get }
}

enum ServerError: ApplicationError {
    case ServerError(message: String?)
    case Unexpected
    
    func local() -> String {
        switch self {
        case .ServerError(let message):
            return message ?? "Server error"
        case .Unexpected:
            return "Unexpected server error"
        }
    }
}

enum ResponseStatusCode {
    case UNAUTHORIZED
    case BAD_REQUEST(message: String?)
    case NOT_FOUND(message: String?)
    case FORBIDDEN
    case INTERNAL_SERVER_ERROR
    case UNACCEPTABLE
    
    init(code: Int, message: String?){
        switch code {
        case 401:
            self = .UNAUTHORIZED
        case 400:
            self = .BAD_REQUEST(message: message)
        case 403:
            self = .FORBIDDEN
        case 404:
            self = .NOT_FOUND(message: message)
        case 500:
            self = .INTERNAL_SERVER_ERROR
        default:
            self = .UNACCEPTABLE
        }
    }
    
    init(code: Int, data: Data? = nil){
        var response: ServerErrorResponse?
        if let data = data {
            response = CodableService.decode(data: data)
        }
        self.init(code: code, message: response?.error.message)
    }
    
    func serverError() -> ServerError{
       switch self {
       case .BAD_REQUEST(let message), .NOT_FOUND(let message):
            return .ServerError(message: message)
       case .UNAUTHORIZED, .FORBIDDEN, .INTERNAL_SERVER_ERROR, .UNACCEPTABLE:
            return .ServerError(message: nil)
        }
    }
}

struct ServerErrorResponse: Codable {
    var error: ServerErrorMessage
}

struct ServerErrorMessage: Codable {
    var message: String
}
