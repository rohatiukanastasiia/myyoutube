//
//  YoutubeService.swift
//  myYoutube
//
//  Created by Nastya Rogatuk on 18.06.2020.
//  Copyright © 2020 AR. All rights reserved.
//

import Foundation
import Alamofire

class YoutubeService {
    
    static func getChannels(ids: [String], success:@escaping((ChannelsResponse)->()), fail:@escaping(ServerErrorHandler)){
        YoutubeEndpoint.channels(ids: ids).fetch(success: success, fail: fail)
    }
    
    static func getPlaylists(for channelId: String, maxResults: UInt, success:@escaping((PlayListResponse)->()), fail:@escaping(ServerErrorHandler)){
        YoutubeEndpoint.playlists(id: channelId, maxResults: maxResults).fetch(success: success, fail: fail)
    }
    
    static func getPlaylistItems(for playlistId: String, maxResults: UInt, success:@escaping((PlayListItemResponse)->()), fail:@escaping(ServerErrorHandler)){
        YoutubeEndpoint.playlistItems(id: playlistId, maxResults: maxResults).fetch(success: success, fail: fail)
    }
    
    static func getVideoItems(by ids: [String], success:@escaping((VideosReponse)->()), fail:@escaping(ServerErrorHandler)){
        YoutubeEndpoint.videos(ids: ids).fetch(success: success, fail: fail)
    }
}

private enum YoutubeEndpoint: APIConfiguration {
    
    case channels(ids: [String])
    case playlists(id: String, maxResults: UInt)
    case playlistItems(id: String, maxResults: UInt)
    case videos(ids: [String])
    
    var method: HTTPMethod {
        switch self {
        case .channels, .playlists, .playlistItems, .videos:
            return HTTPMethod.get
        }
    }
    
    var path: String {
        switch self {
        case .channels:
            return "channels"
        case .playlists:
            return "playlists"
        case .playlistItems:
            return "playlistItems"
        case .videos:
            return "videos"
        }
    }
    
    var query: Parameters? {
        switch self {
        case .channels(let ids):
            return ["id": ids.joined(separator: ","),
                    "part": "snippet,brandingSettings,statistics",
                    "key": YoutubeApiConfiguration.apiKey]
        case .playlists(let id, let maxResults):
            return ["channelId": id,
                    "maxResults": maxResults,
                    "part": "contentDetails,snippet",
                    "key": YoutubeApiConfiguration.apiKey]
        case .playlistItems(let id, let maxResults):
            return ["playlistId": id,
                    "maxResults": maxResults,
                    "part": "snippet",
                    "key": YoutubeApiConfiguration.apiKey]
        case .videos(let ids):
            return ["id": ids.joined(separator: ","),
                    "part": "contentDetails,snippet,statistics",
                    "key": YoutubeApiConfiguration.apiKey]
        }
    }
}

//channel info model
struct ChannelsResponse: Codable {
    var items: [ChannelItem]
}

struct ChannelItem: Codable {
    var id: String
    var snippet: Snippet
    var brandingSettings: ChannelBrandingSettings
    var statistics: ChannelStatistics
}

struct Snippet: Codable {
    var title: String
}

struct ChannelStatistics: Codable {
    var subscriberCount: String
    
    var subscriberDescription: String {
        return "\(subscriberCount) подписчика"
    }
}

struct ChannelBrandingSettings: Codable {
    var image: ChannelImage
}

struct ChannelImage: Codable {
    var bannerImageUrl: String
    var bannerMobileImageUrl: String
    
    var bannerImage: URL? {
        return URL(string: bannerMobileImageUrl)
    }
}

//playlist info model
struct PlayListResponse: Codable {
    var items: [PlayListInfoItem]
}

struct PlayListInfoItem: Codable {
    var id: String
    var snippet: Snippet
    
    var lists: [PlayListItem]?
}

//playListItem model
struct PlayListItemResponse: Codable {
    var items: [PlayListItem]
}

struct PlayListItem: Codable {
    var id: String
    var snippet: PlayListItemSnippet
    
    var videos: [Video]?
}

struct PlayListItemSnippet: Codable {
    var title: String
    var thumbnails: Thumbnails
    var resourceId: PlayListItemResource
}

struct PlayListItemResource: Codable {
    var videoId: String
}

struct Thumbnails: Codable {
    var standard: ThumbnailItem?
}

struct ThumbnailItem: Codable {
    var url: String
    var width: CGFloat
    var height: CGFloat
    
    var bannerImage: URL? {
        return URL(string: url)
    }
}

struct VideosReponse: Codable {
    var items: [Video]
}

struct Video: Codable {
    var id: String
    var statistics: VideoStatistics
}

struct VideoStatistics: Codable {
    var viewCount: String
    
    var viewCountDescription: String {
        return "\(viewCount) просмотров"
    }
}
