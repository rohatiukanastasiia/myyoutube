//
//  BaseNetworkingConnection.swift
//  myYoutube
//
//  Created by Nastya Rogatuk on 18.06.2020.
//  Copyright © 2020 AR. All rights reserved.
//

import Foundation
import Alamofire

extension APIConfiguration {
    
    func fetch<Entity: Codable>(success:@escaping(ServerSuccessHandlerResult<Entity>), fail:@escaping(ServerErrorHandler)){
        call(success: { data  in
            CodableService.decode(data: data, success: success, fail: fail)
        }, fail: fail)
    }
}

extension APIConfiguration {
    
    func call(success:@escaping(SuccessResponse), fail:@escaping(ServerErrorHandler)) {
        do {
            try self.dataRequest().validate().responseData{ (response) in
            switch response.result {
                case .success(let data):
                    success(data)
                case .failure(let error):
                    do {
                        try self.handlerFailureResponse(error: error, data: response.data)
                    } catch let error as ServerError {
                        fail(error)
                    } catch {
                        fail(ServerError.Unexpected)
                    }
                }
            }
        } catch {
            fail(ServerError.Unexpected)
        }
    }
    
    private func handlerFailureResponse(error: AFError, data: Data?) throws {
        switch error {
        case .responseValidationFailed(let reason):
            switch reason {
            case .unacceptableStatusCode(let code):
                throw ResponseStatusCode(code: code, data: data).serverError()
            default:
                break
            }
        default:
            throw ServerError.Unexpected
        }
    }
}

extension APIConfiguration {
    
    fileprivate func dataRequest() throws -> DataRequest {
        let url = EnvironmentConfiguration.host
        return AF.request(url.appendingPathComponent(path), method: method, parameters: query, encoding: URLEncoding.queryString, headers: headers, interceptor: Interceptor.shared)
    }
}

extension APIConfiguration {
    
    var headers: HTTPHeaders {
        var headers: HTTPHeaders = [:]
        if let language = Locale.preferredLanguages.first?.lowercased() {
            headers.add(name: "Accept-Language", value: language)
        }
        return headers
    }
}

private class Interceptor: RequestInterceptor {
    
    static let shared = Interceptor()
    private init(){}
    
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        //this method for authorizationToken if needed
        completion(.success(urlRequest))
    }
}
