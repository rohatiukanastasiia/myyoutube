//
//  BuildConfiguration.swift
//  myYoutube
//
//  Created by Nastya Rogatuk on 19.06.2020.
//  Copyright © 2020 AR. All rights reserved.
//

import Foundation

enum BuildConfiguration: String {
    
    case networkEnvironment = "EnvironmentHost"
    case youtubeApiKey = "ApiKey"
    
    enum Error: Swift.Error {
        case missingKey, invalidValue
    }

    func value<T>() throws -> T where T: LosslessStringConvertible {
        guard let object = Bundle.main.object(forInfoDictionaryKey:self.rawValue) else {
            throw Error.missingKey
        }

        switch object {
        case let value as T:
            return value
        case let string as String:
            guard let value = T(string) else { fallthrough }
            return value
        default:
            throw Error.invalidValue
        }
    }
}

class EnvironmentConfiguration {
    
    static var host : URL {
        let path: String = try! BuildConfiguration.networkEnvironment.value()
        return URL(string: path)!
    }
}

class YoutubeApiConfiguration {
    
    static var apiKey : String {
        return try! BuildConfiguration.youtubeApiKey.value()
    }
}
