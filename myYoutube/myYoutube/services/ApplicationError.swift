//
//  ApplicationError.swift
//  myYoutube
//
//  Created by Nastya Rogatuk on 18.06.2020.
//  Copyright © 2020 AR. All rights reserved.
//

import Foundation

protocol ApplicationError: Error {
    func local() -> String
}
