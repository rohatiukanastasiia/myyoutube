//
//  ChannelsHeaderCell.swift
//  myYoutube
//
//  Created by Nastya Rogatuk on 18.06.2020.
//  Copyright © 2020 AR. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class ChannelsHeaderCell: UITableViewCell {
    
    @IBOutlet weak var pageControll: UIPageControl!
    @IBOutlet weak var bannerView: CornerViews!{
        didSet{
            bannerView.backgroundColor = AppColor.bannerBackgroundColor.color()
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onBannerTap))
            bannerView.addGestureRecognizer(tap)
        }
    }
    @IBOutlet weak var channelNameLabel: UILabel!{
        didSet{
             channelNameLabel.textStyle(.channelTitle)
        }
    }
    @IBOutlet weak var channelSubscriberLabel: UILabel!{
        didSet{
            channelSubscriberLabel.textStyle(.channelSubscriber)
        }
    }
    @IBOutlet weak var bannerImageView: UIImageView!
    
    var channels: [ChannelItem] = [] {
        didSet{
            reload()
        }
    }
    var onChannelSelect: ((ChannelItem) -> ())?
    
    private var bannerTimer: Timer?
    private var timerCount: Int = 0 {
        didSet{
            guard !channels.isEmpty else {return}
            currentChannelIndex = timerCount % self.channels.count
        }
    }
    private var currentChannelIndex: Int? {
        didSet{
            guard let index = currentChannelIndex else {return}
            pageControll.currentPage = index
            
            let channel = channels[index]
            channelNameLabel.text = channel.snippet.title
            channelSubscriberLabel.text = channel.statistics.subscriberDescription
            
            if let bannerURL = channel.brandingSettings.image.bannerImage {
                bannerImageView.sd_setImage(with: bannerURL) { (img, error, type, url) in
                }
            }
        }
    }
    
    private func reload(){
        bannerTimer?.invalidate()
        timerCount = 0
        bannerTimer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true){ [weak self] timer in
            self?.timerCount += 1
        }
    }
    
    @objc private func onBannerTap(){
        guard let index = currentChannelIndex else {return}
        let channel = channels[index]
        onChannelSelect?(channel)
    }
    
    override func prepareForReuse() {
        bannerTimer?.invalidate()
    }
    
    deinit {
        bannerTimer?.invalidate()
    }
}
