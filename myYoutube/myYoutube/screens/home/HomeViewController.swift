//
//  HomeViewController.swift
//  myYoutube
//
//  Created by Nastya Rogatuk on 18.06.2020.
//  Copyright © 2020 AR. All rights reserved.
//

import UIKit

protocol HomeViewDelegate {
    func fetchChannels()
    func fetchPlaylist(for channel: ChannelItem)
}

class HomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var delegate: HomeViewDelegate?
    private var channels: [ChannelItem] = [] {
        didSet{
            tableView.reloadData()
        }
    }
    private var selectedChannel: ChannelItem? {
        didSet{
            guard let selected = selectedChannel else {return}
            delegate?.fetchPlaylist(for: selected)
        }
    }
    
    private var playLists: [PlayListInfoItem] = [] {
        didSet{
            tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HomeConfigurator.shared.configure(vc: self)
        navigationBarSettings()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        delegate?.fetchChannels()
    }
    
    private func navigationBarSettings(){
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.title = "YouTube API"
    }
}

extension HomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return channels.isEmpty ? 0 : 1
        } else {
            return playLists.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.section != 0 else {
            return configurateTopRow(tableView, cellForRowAt: indexPath)
        }
        return configuratePlaylistCell(tableView, cellForRowAt: indexPath)
    }
    
    private func configurateTopRow(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> ChannelsHeaderCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "channelsHeader", for: indexPath) as! ChannelsHeaderCell
        cell.channels = channels
        cell.onChannelSelect = {[weak self] channel in
            self?.selectedChannel = channel
        }
        return cell
    }
    
    private func configuratePlaylistCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> PlayListCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "playList", for: indexPath) as! PlayListCell
        cell.playList = playLists[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
}

extension HomeViewController: UITableViewDelegate {
    
}

extension HomeViewController: HomePresenterDelegate {
    func showUpdate(playlist: PlayListInfoItem) {
        if let index = self.playLists.firstIndex(where: { (oldPlaylist) -> Bool in
            return oldPlaylist.id == playlist.id
        }) {
            self.playLists[index] = playlist
        }
    }
    
    func showUpdate(playlistItem: PlayListItem) {
        var up = self.playLists[0]
        up.lists?[0] = playlistItem
    }
    
    func showPlaylists(playlists: [PlayListInfoItem]) {
        self.playLists = playlists
    }
    
    func showChannels(channels: [ChannelItem]) {
        self.channels = channels
    }
}
