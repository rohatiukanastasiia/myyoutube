//
//  HomePresenter.swift
//  myYoutube
//
//  Created by Nastya Rogatuk on 18.06.2020.
//  Copyright © 2020 AR. All rights reserved.
//

import Foundation

protocol HomePresenterDelegate: class {
    func showChannels(channels: [ChannelItem])
    func showPlaylists(playlists: [PlayListInfoItem])
    func showUpdate(playlist: PlayListInfoItem)
    func showUpdate(playlistItem: PlayListItem)
}

class HomePresenter: HomeInteractorDelegate {
    
    weak var delegate: HomePresenterDelegate?
    
    func presentResponseChannels(result: ChannelsResponse) {
        delegate?.showChannels(channels: result.items)
    }
    
    func presentResponsePlaylists(result: PlayListResponse) {
        delegate?.showPlaylists(playlists: result.items)
    }
    
    func presentResponsePlaylistItems(result: PlayListItemResponse, for playlists: PlayListInfoItem) {
        var updated = playlists
        updated.lists = result.items
        delegate?.showUpdate(playlist: updated)
    }
    
    func presentResponseVideoItems(result: VideosReponse, for playlist: PlayListItem) {
        var updated = playlist
        updated.videos = result.items
        delegate?.showUpdate(playlistItem: updated)
    }
}
