//
//  VideoItemCell.swift
//  myYoutube
//
//  Created by Nastya Rogatuk on 23.06.2020.
//  Copyright © 2020 AR. All rights reserved.
//

import Foundation
import UIKit

class VideoItemCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerView: UIImageView!
    @IBOutlet weak var videoTitle: UILabel!{
        didSet{
            videoTitle.textStyle(.videoName)
        }
    }
    @IBOutlet weak var videoViews: UILabel!{
        didSet{
            videoViews.textStyle(.videoViews)
        }
    }
    
    func configurate(item: PlayListItem) {
        videoTitle.text = item.snippet.title
        videoViews.text = item.videos?.first?.statistics.viewCountDescription
        
        if let bannerURL = item.snippet.thumbnails.standard?.bannerImage {
            bannerView.sd_setImage(with: bannerURL) { (img, error, type, url) in
            }
        }
    }
}
