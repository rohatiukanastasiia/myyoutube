//
//  HomeInteractor.swift
//  myYoutube
//
//  Created by Nastya Rogatuk on 18.06.2020.
//  Copyright © 2020 AR. All rights reserved.
//

import Foundation

protocol HomeInteractorDelegate {
    func presentResponseChannels(result: ChannelsResponse)
    func presentResponsePlaylists(result: PlayListResponse)
    func presentResponsePlaylistItems(result: PlayListItemResponse, for playlists: PlayListInfoItem)
    func presentResponseVideoItems(result: VideosReponse, for playlist: PlayListItem)
}

class HomeInteractor: HomeViewDelegate {
    
    var delegate: HomeInteractorDelegate?
    let listOfChannelsId = ["UCAptZHV2-xkB4GJGgSKRsHQ","UCjJ-oj2aJTINILYLDv2MFEQ","UCLIXIfi3GguS5f2C-mfh7Tg"]
    
    func fetchChannels() {
        YoutubeService.getChannels(ids: listOfChannelsId, success: { result in
            self.delegate?.presentResponseChannels(result: result)
        }) { (error) in
            print(error.local())
        }
    }
    
    func fetchPlaylist(for channel: ChannelItem) {
        //TODO: loading
        YoutubeService.getPlaylists(for: channel.id, maxResults: 2,  success: { result in
            self.delegate?.presentResponsePlaylists(result: result)
            self.fetchPlaylistItems(for: result.items)
        }) { (error) in
            print(error.local())
        }
    }
    
    func fetchPlaylistItems(for playlists: [PlayListInfoItem]) {
        let ids = playlists.compactMap{$0.id}
        YoutubeService.getPlaylistItems(for: ids.first!, maxResults: 10, success: { result in
            self.delegate?.presentResponsePlaylistItems(result: result, for: playlists.first!)
            self.fetchVideos(for: result.items.first!)
        }) { (error) in
            print(error.local())
        }
    }
    
    func fetchVideos(for playlist: PlayListItem) {
        YoutubeService.getVideoItems(by: [playlist.snippet.resourceId.videoId], success: { result in
            self.delegate?.presentResponseVideoItems(result: result, for: playlist)
        }) { (error) in
            print(error.local())
        }
    }
}
