//
//  HomeConfigurator.swift
//  myYoutube
//
//  Created by Nastya Rogatuk on 18.06.2020.
//  Copyright © 2020 AR. All rights reserved.
//

import Foundation

class HomeConfigurator{
    
    static let shared = HomeConfigurator()
    private init(){}

    func configure(vc: HomeViewController){
        let interactor = HomeInteractor()
        let presenter = HomePresenter()
        vc.delegate = interactor
        interactor.delegate = presenter
        presenter.delegate = vc
    }
}
