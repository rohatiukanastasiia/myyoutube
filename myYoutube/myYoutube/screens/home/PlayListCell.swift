//
//  PlayListCell.swift
//  myYoutube
//
//  Created by Nastya Rogatuk on 18.06.2020.
//  Copyright © 2020 AR. All rights reserved.
//

import Foundation
import UIKit

class PlayListCell: UITableViewCell {
    
    @IBOutlet weak var playListTitleLabel: UILabel!{
        didSet{
            playListTitleLabel.textStyle(.playListTitle)
        }
    }
    @IBOutlet weak var collectionList: UICollectionView!
    
    var playList: PlayListInfoItem? {
        didSet{
            guard let playList = playList else {return}
            playListTitleLabel.text = playList.snippet.title
            collectionList.reloadData()
        }
    }
}

extension PlayListCell: UICollectionViewDelegate {
    
}

extension PlayListCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playList?.lists?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCell", for: indexPath) as! VideoItemCell
        if let item = playList?.lists?[indexPath.row] {
            cell.configurate(item: item)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
